import numpy
import data


def process_sentence(sentences):
    """
    Change sentences into a torch array of word ids
    The shorter sentences will be padded with #oov#, with an idx indicating the end of sentence
    :param sentences: 
    :return: 
    """
    word_dict, _, _, _, _, _ = data.generate_word_dictionary()
    batch_size = len(sentences)
    max_sentence_length = max([len(s.split(' ')) for s in sentences])
    input_word_id = numpy.zeros((batch_size, max_sentence_length - 1))
    idx = numpy.zeros(batch_size)
    output_word_id = numpy.zeros(batch_size)
    for i in range(len(sentences)):
        sentence = sentences[i].split(' ')
        sentence = [word_dict.get(word) for word in sentence]
        sentence = numpy.asarray(sentence).astype(int)
        input_word_id[i, :] = word_dict['#oov#']
        idx[i] = len(sentence) - 1
        input_word_id[i, 0:len(sentence) - 1] = sentence[0:-1]
        output_word_id[i] = sentence[-1]
    return input_word_id, idx, output_word_id


def process_question_answer(questions, answers):
    """
    Change questions into a torch array of word ids
    The shorter questions will be padded with #oov#, with an idx indicating the end of question
    :param questions: 
    :param answers:
    :return: 
    """
    word_dict, _, _, _, _, _ = data.generate_word_dictionary()
    batch_size = len(questions)
    max_question_length = max([len(s.split(' ')) for s in questions])
    input_word_id = numpy.zeros((batch_size, max_question_length))
    idx = numpy.zeros(batch_size)
    output_word_id = numpy.zeros(batch_size)
    for i in range(len(questions)):
        question = questions[i].split(' ')
        question = [word_dict.get(word) for word in question]
        question = numpy.asarray(question).astype(int)
        input_word_id[i, :] = word_dict['#oov#']
        idx[i] = len(question)
        input_word_id[i, 0:len(question)] = question
        answer = word_dict.get(answers[i])
        output_word_id[i] = answer
    return input_word_id, idx, output_word_id


def unit_test():
    word_dict, object_dict, color_dict, _, _, _ = data.generate_word_dictionary()
    object_center = data.generate_cluster_center(len(object_dict), 10)
    color_center = data.generate_cluster_center(len(color_dict), 8)
    for difficulty in range(11):
        _, sentence, question, answer, object_label, color_label = \
            data.generate_data(object_center, color_center, difficulty, 10)
        print('difficulty %d' % difficulty)
        input_word_id, idx, output_word_id = process_sentence(sentence)
        print(sentence)
        print(input_word_id)
        print(idx)
        print(output_word_id)
        input_word_id, idx, output_word_id = process_question_answer(question, answer)
        print(question)
        print(answer)
        print(input_word_id)
        print(idx)
        print(output_word_id)


if __name__ == '__main__':
    unit_test()
