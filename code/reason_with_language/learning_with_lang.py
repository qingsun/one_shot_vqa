from __future__ import division

import numpy
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.nn.parameter import Parameter

import data
import preprocess


class Net3(nn.Module):
    def __init__(self, n_object, n_location, n_other, img_dim, img_size):
        super(Net3, self).__init__()
        self.n_words = n_object + n_location + n_other
        self.extra_dim = img_dim
        self.extra_feature = Parameter(torch.rand(1, self.extra_dim, img_size, img_size))
        self.emb_dim = img_dim+self.extra_dim
        self.emb = Parameter(torch.rand(self.n_words, self.emb_dim))
        self.emb_bias = Parameter(torch.rand(self.n_words))
        self.hidden_dim = 32
        self.hidden = nn.Linear(self.hidden_dim+self.emb_dim, self.hidden_dim)
        self.output_gate = nn.Linear(self.hidden_dim, self.emb_dim)
        self.programmer_size = 2
        self.prog_hidden = nn.Linear(self.hidden_dim+self.emb_dim, self.hidden_dim)
        self.prog_key = nn.Linear(self.hidden_dim, self.hidden_dim)
        self.prog_key_bias = nn.Linear(self.hidden_dim, 1)

    def forward(self, image_feature, input_word_id, idx):
        # First go through the whole sentence and get their word embedding
        [batch_size, num_words] = input_word_id.size()
        word_emb_weight = Variable(torch.zeros(batch_size, num_words, self.emb_dim))
        word_emb_bias = Variable(torch.zeros(batch_size, num_words))
        for i in range(num_words):
            for j in range(batch_size):
                word_emb_weight[j, i, :] = self.emb[input_word_id.data[j, i], :]
                word_emb_bias[j, i] = self.emb_bias[input_word_id.data[j, i]]
        # Second go through the whole sentence and get sentence embedding
        init_hidden = Variable(torch.zeros(batch_size, self.hidden_dim))
        cumulative_hidden = Variable(torch.zeros(batch_size, num_words, self.hidden_dim))
        for i in range(num_words):
            emb = word_emb_weight[:, i, :]
            if i == 0:
                hidden = F.relu(self.hidden(torch.cat((init_hidden, emb), 1)))
            else:
                hidden = F.relu(self.hidden(torch.cat((hidden, emb), 1)))
            cumulative_hidden[:, i, :] = hidden
        hidden = Variable(torch.zeros(batch_size, self.hidden_dim))
        for j in range(batch_size):
            hidden[j, :] = cumulative_hidden[j, idx.data[j] - 1, :]
        # Now compute neural program interpreter
        programmer_weight = Variable(torch.zeros(batch_size, self.programmer_size, self.emb_dim))
        programmer_bias = Variable(torch.zeros(batch_size, self.programmer_size))
        init_prog = Variable(torch.zeros(batch_size, self.emb_dim))
        for i in range(self.programmer_size):
            if i == 0:
                prog_hidden = F.relu(self.prog_hidden(torch.cat((hidden, init_prog), 1)))
            else:
                prog_hidden = F.relu(self.prog_hidden(torch.cat((prog_hidden, programmer_weight[:, i-1, :]), 1)))
            prog_key = self.prog_key(prog_hidden)
            prog_key_bias = self.prog_key_bias(prog_hidden)
            # tmp = word_emb_weight * prog_key.unsqueeze(1).expand(batch_size, num_words, self.emb_dim)
            tmp = cumulative_hidden * prog_key.unsqueeze(1).expand(batch_size, num_words, self.hidden_dim)
            word_attn = tmp.sum(2).squeeze(2) + word_emb_bias * prog_key_bias.expand_as(word_emb_bias)
            word_attn = F.softmax(word_attn)
            programmer_weight[:, i, :] = (word_emb_weight * word_attn.unsqueeze(2).expand(batch_size, num_words, self.emb_dim)).sum(1)
            programmer_bias[:, i] = (word_emb_bias * word_attn).sum(1)
        # Now go through the image and every word in the sentence
        [_, _, height, width] = image_feature.size()
        extra_feature = self.extra_feature.expand(batch_size, self.extra_dim, height, width)
        concat_feature = torch.cat((image_feature, extra_feature), 1)
        attn = Variable(torch.zeros((batch_size, 1, height, width)))
        attn[:, :, int((height - 1) / 2), int((width - 1) / 2)] = 1
        # cumulative_attn = Variable(torch.zeros((batch_size, num_words, height, width)))
        inv_idx = Variable(torch.range(height * width - 1, 0, -1).long())
        for i in range(self.programmer_size):
            img_score = Variable(torch.zeros(batch_size, 1, height, width))
            # conv_filter = word_emb_weight[:, i, :].contiguous().view(batch_size, -1, 1, 1)
            # conv_bias = word_emb_bias[:, i]
            conv_filter = programmer_weight[:, i, :].contiguous().view(batch_size, -1, 1, 1)
            conv_bias = programmer_bias[:, i]
            for j in range(batch_size):
                img_score[j, :, :, :] = F.conv2d(concat_feature[j, :, :, :].unsqueeze(0),
                                                 conv_filter[j, :, :, :].unsqueeze(0), conv_bias[j])
            kernel = F.softmax(img_score.view(batch_size, -1))
            kernel = kernel.index_select(1, inv_idx)
            kernel = kernel.view(-1, 1, height, width)
            last_attn = attn
            attn = Variable(torch.zeros((batch_size, 1, height, width)))
            for j in range(batch_size):
                attn[j, :, :, :] = F.conv2d(last_attn[j, :, :, :].unsqueeze(0),
                                            kernel[j, :, :, :].unsqueeze(0), None, 1, 1)
            sum_attn = attn.view(batch_size, -1).sum(1)
            attn = attn / sum_attn.view(-1, 1, 1, 1).expand_as(attn)
            # cumulative_attn[:, i, :, :] = attn
        # attn = Variable(torch.zeros(batch_size, 1, height, width))
        # for j in range(batch_size):
        #     attn[j, :, :, :] = cumulative_attn[j, idx.data[j]-1, :, :]
        x = concat_feature * attn.expand_as(concat_feature)
        x = x.sum(3).sum(2).squeeze(3).squeeze(2)
        output_gate = F.sigmoid(self.output_gate(hidden))
        x = x * output_gate
        y = torch.mm(x, self.emb.transpose(0, 1)) + self.emb_bias.unsqueeze(0).expand(batch_size, self.n_words)
        return y, attn


class Net2(nn.Module):
    def __init__(self, n_words, n_dim, img_size):
        super(Net2, self).__init__()
        self.conv = nn.Conv2d(n_dim, n_words, 1)
        self.emb = nn.Embedding(n_words, 32)
        self.hidden = nn.Linear(32, 32)
        self.emb_to_loc_kernel = nn.Linear(32, img_size**2)
        self.emb_to_img_kernel = nn.Linear(32, n_dim)
        self.gate = nn.Linear(32, 1)

    def forward(self, image_feature, input_word_id, idx):
        [batch_size, img_dim, height, width] = image_feature.size()
        num_words = input_word_id.size(1)
        attn = Variable(torch.zeros((batch_size, 1, height, width)))
        attn[:, :, int((height - 1) / 2), int((width - 1) / 2)] = 1
        cumulative_attn = Variable(torch.zeros((batch_size, num_words, height, width)))
        inv_idx = Variable(torch.range(height * width - 1, 0, -1).long())
        for i in range(num_words):
            img_score = self.conv(image_feature)
            img_attn = Variable(torch.zeros((batch_size, 1, height, width)))
            for j in range(batch_size):
                img_attn[j, :, :, :] = img_score[j, input_word_id.data[j, i], :, :]
            img_attn = F.softmax(img_attn.view(batch_size, -1))
            img_attn = img_attn.index_select(1, inv_idx)
            img_attn = img_attn.view(-1, 1, height, width)
            emb = self.emb(input_word_id[:, i])
            hidden = F.relu(self.hidden(emb))
            loc_kernel = F.softmax(self.emb_to_loc_kernel(hidden)).view(-1, 1, height, width)
            gate = F.sigmoid(self.gate(hidden))
            gate = gate.unsqueeze(2).unsqueeze(3).expand_as(attn)
            kernel = gate * img_attn + (1 - gate) * loc_kernel
            last_attn = attn
            attn = Variable(torch.zeros((batch_size, 1, height, width)))
            for j in range(batch_size):
                attn[j, :, :, :] = F.conv2d(last_attn[j, :, :, :].unsqueeze(0),
                                            kernel[j, :, :, :].unsqueeze(0), None, 1, 1)
            sum_attn = attn.view(batch_size, -1).sum(1)
            attn = attn / sum_attn.view(-1, 1, 1, 1).expand_as(attn)
            cumulative_attn[:, i, :, :] = attn
        attn = Variable(torch.zeros(batch_size, 1, height, width))
        for j in range(batch_size):
            attn[j, :, :, :] = cumulative_attn[j, idx.data[j] - 1, :, :]
        x = image_feature * attn.expand_as(image_feature)
        x = x.sum(3).sum(2)
        y = self.conv(x).squeeze(3).squeeze(2)
        return y, attn


class Net(nn.Module):
    def __init__(self, n_object, n_location, n_other, n_dim, img_size):
        super(Net, self).__init__()
        self.emb = nn.Embedding(n_words, 16)
        self.hidden = nn.Linear(16, 16)
        self.emb_to_kernel = nn.Linear(16, img_size**2)
        self.conv = nn.Conv2d(n_dim, n_words, 1)

    def forward(self, image_feature, input_word_id, idx):
        [batch_size, img_dim, height, width] = image_feature.size()
        num_words = input_word_id.size(1)
        attn = Variable(torch.zeros((batch_size, 1, height, width)))
        attn[:, :, int((height - 1) / 2), int((width - 1) / 2)] = 1
        cumulative_attn = Variable(torch.zeros((batch_size, num_words, height, width)))
        for i in range(num_words):
            emb = self.emb(input_word_id[:, i])
            hidden = F.relu(self.hidden(emb))
            kernel = F.softmax(self.emb_to_kernel(hidden)).view(-1, 1, height, width)
            last_attn = attn
            attn = Variable(torch.zeros((batch_size, 1, height, width)))
            for j in range(batch_size):
                attn[j, :, :, :] = F.conv2d(last_attn[j, :, :, :].unsqueeze(0),
                                            kernel[j, :, :, :].unsqueeze(0), None, 1, 1)
            sum_attn = attn.view(-1, height*width).sum(1)
            attn = attn / sum_attn.view(-1, 1, 1, 1).expand_as(attn)
            cumulative_attn[:, i, :, :] = attn
        attn = Variable(torch.zeros(batch_size, 1, height, width))
        for j in range(batch_size):
            attn[j, :, :, :] = cumulative_attn[j, idx.data[j]-1, :, :]
        x = image_feature * attn.expand_as(image_feature)
        x = x.sum(3).sum(2)
        y = self.conv(x).squeeze(3).squeeze(2)
        return y, attn


class SimpleNet(nn.Module):
    def __init__(self, n_words, n_dim, img_size):
        super(SimpleNet, self).__init__()
        self.emb = nn.Embedding(n_words, 16)
        self.emb_to_kernel = nn.Linear(16, img_size**2)
        self.fc = nn.Linear(n_dim, n_words)
        self.img_size = img_size

    def forward(self, image_feature, input_word_id):
        emb = F.relu(self.emb(input_word_id[:, 0]))
        kernel = F.softmax(self.emb_to_kernel(emb).view(-1, self.img_size**2))
        kernel = kernel.view(-1, 1, self.img_size, self.img_size)
        x = image_feature * kernel.expand_as(image_feature)
        x = x.sum(3).sum(2).squeeze(3).squeeze(2)
        y = self.fc(x)
        return y, kernel


def train(model, cluster_center, difficulty, n_epoch=1000):
    optimizer = optim.Adam(model.parameters(), lr=0.01)
    for epoch in range(n_epoch):
        batch = data.generate_data(cluster_center, difficulty)
        image_feature = Variable(torch.from_numpy(batch[0])).float()
        input_word_id, idx, output_word_id = preprocess.process_sentence(batch[1])
        input_word_id = Variable(torch.from_numpy(input_word_id)).long()
        idx = Variable(torch.from_numpy(idx)).long()
        output_word_id = Variable(torch.from_numpy(output_word_id)).long()
        optimizer.zero_grad()
        pred, _ = model(image_feature, input_word_id, idx)
        loss = F.cross_entropy(pred, output_word_id)
        loss.backward()
        optimizer.step()
        print('Train Epoch: {}\tLoss: {:.6f}'.format(epoch, loss.data[0]))
    return model


def test(model, cluster_center, difficulty, n_epoch=10):
    test_accuracy = []
    test_attn_accuracy = []
    for epoch in range(n_epoch):
        batch = data.generate_data(cluster_center, difficulty)
        image_feature = Variable(torch.from_numpy(batch[0])).float()
        input_word_id, idx, output_word_id = preprocess.process_sentence(batch[1])
        input_word_id = Variable(torch.from_numpy(input_word_id)).long()
        idx = Variable(torch.from_numpy(idx)).long()
        output_word_id = Variable(torch.from_numpy(output_word_id)).long()
        sentences = batch[1]
        questions, answers = batch[2], batch[3]
        object_label, color_label = batch[4], batch[5]
        output_location = preprocess.get_groundtruth_location(output_word_id, object_label, color_label)
        output_location = Variable(torch.from_numpy(batch[4])).long()
        pred, attn = model(image_feature, input_word_id, idx)
        attn = attn.view(attn.size(0), -1)
        pred = pred.data.max(1)[1]
        attn = attn.data.max(1)[1]
        accuracy = pred.eq(output_word_id.data).cpu().sum() * 1.0 / pred.size(0)
        attn_accuracy = attn.eq(output_location.data).cpu().sum() * 1.0 / attn.size(0)
        test_accuracy.append(accuracy)
        test_attn_accuracy.append(attn_accuracy)
    return numpy.mean(numpy.asarray(test_accuracy)), numpy.mean(numpy.asarray(test_attn_accuracy))


def main():
    word_dict, object_dict, color_dict, _, _, _ = data.generate_word_dictionary()
    object_center = data.generate_cluster_center(len(object_dict), 10)
    color_center = data.generate_cluster_center(len(color_dict), 8)
    img_size = 3  # Currently only support image with size 3x3
    model = SimpleNet(len(word_dict), object_center.shape[1]+color_center.shape[1], img_size)
    # Curriculum learning
    for difficulty in range(11):
        model = train(model, object_center, color_center, difficulty)
        test_accuracy, test_attn_accuracy = test(model, object_center, color_center, difficulty)
        print('average accuracy: %.2f' % test_accuracy)
        print('average attention accuracy: %.2f' % test_attn_accuracy)

if __name__ == '__main__':
    main()
