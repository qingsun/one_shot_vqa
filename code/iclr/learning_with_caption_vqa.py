from __future__ import division

import numpy
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.nn.parameter import Parameter

import data
import preprocess


class Net3(nn.Module):
    def __init__(self, paras):
        super(Net3, self).__init__()

        self.extra_dim = paras['emb_dim'] - paras['img_dim']
        self.extra_feature = Parameter(torch.rand(1, self.extra_dim, paras['img_size'], paras['img_size']))
        # embedding variables
        self.emb_dim, self.n_words = paras['emb_dim'], paras['n_words'],
        self.emb = Parameter(torch.rand(self.n_words + 1, self.emb_dim))
        self.emb_bias = Parameter(torch.rand(self.n_words + 1))
        # Attentional RNNs [Bengio, iclr, 2015] ~= neural programmer
        self.encoder_hidden_dim, self.decoder_hidden_dim = paras['encoder_hidden_dim'], paras['decoder_hidden_dim']
        ## -- encoder --
        self.encoder = nn.Linear(self.encoder_hidden_dim+self.emb_dim, self.encoder_hidden_dim)
        self.programmer_size = 2
        ## -- decoder --
        self.decoder = nn.Linear(self.decoder_hidden_dim+self.emb_dim, self.decoder_hidden_dim)
        ### -- attention --
        self.prog_key = nn.Linear(self.decoder_hidden_dim, self.encoder_hidden_dim)
        # self.prog_key_bias = nn.Linear(self.hidden_dim, 1)
        self.message = nn.Linear(self.decoder_hidden_dim + 1, 1)
        self.output_gate = nn.Linear(self.encoder_hidden_dim, self.emb_dim)

    def attention(self, hidden, M):
        # M : (batch_size, num_words, self.emb_dim)
        # hidden : (batch_size, self.emb_dim)
        key = F.relu(self.prog_key(hidden))
        cos = nn.CosineSimilarity(dim=2, eps=1e-6)
        distance = cos(M, key.unsqueeze(1).expand(M.shape))
        attn = F.softmax(distance)
        return attn

    def get_kernel(self, raw_kernel):
        # raw_kernel: batch_size, 1, height, width
        # kernel = normalized and flipped raw_kernel
        [batch_size, _, height, width] = raw_kernel.size()
        inv_idx = Variable(torch.range(height * width - 1, 0, -1).long())

        kernel = F.softmax(raw_kernel.view(batch_size, -1))
        kernel = kernel.index_select(1, inv_idx)
        kernel = kernel.view(-1, 1, height, width)

        return kernel

    def forward(self, image_feature, input_word_id, idx, sentence_type, mode):

        # 1) go through the whole sentence and get their word embedding
        [batch_size, num_words] = input_word_id.size()
        word_emb_weight = Variable(torch.zeros(batch_size, num_words, self.emb_dim))
        word_emb_bias = Variable(torch.zeros(batch_size, num_words))
        for i in range(num_words):
            for j in range(batch_size):
                word_emb_weight[j, i, :] = self.emb[input_word_id.data[j, i], :]
                word_emb_bias[j, i] = self.emb_bias[input_word_id.data[j, i]]

        # 2) go through the whole sentence and get sentence embedding
        init_hidden = Variable(torch.zeros(batch_size, self.encoder_hidden_dim))
        cumulative_hidden = Variable(torch.zeros(batch_size, num_words, self.encoder_hidden_dim))
        ## encoder
        for i in range(num_words):
            emb = word_emb_weight[:, i, :]
            if i == 0:
                hidden = F.relu(self.encoder(torch.cat((init_hidden, emb), 1)))
            else:
                hidden = F.relu(self.encoder(torch.cat((hidden, emb), 1)))
            cumulative_hidden[:, i, :] = hidden
        ## get mean hidden vector to init programmer (decoder)
        summary_hidden = Variable(torch.zeros(batch_size, self.encoder_hidden_dim))
        for j in range(batch_size):
            summary_hidden[j, :] = cumulative_hidden[j, :idx.data[j] + 1, :].mean(0)

        # 3) compute neural program interpreter
        programmer_weight = Variable(torch.zeros(batch_size, self.programmer_size, self.emb_dim))
        programmer_bias = Variable(torch.zeros(batch_size, self.programmer_size))
        init_prog = Variable(torch.zeros(batch_size, self.emb_dim))
        ##
        cumulative_prog_hidden = Variable(torch.zeros(batch_size, self.programmer_size, self.decoder_hidden_dim))
        for i in range(self.programmer_size):
            if i == 0:
                prog_hidden = F.relu(self.decoder(torch.cat((summary_hidden, init_prog), 1))) ## here, hidden should be average over hiddens over all time-steps.
            else:
                prog_hidden = F.relu(self.decoder(torch.cat((prog_hidden, programmer_weight[:, i-1, :]), 1)))
            ###
            word_attn = self.attention(prog_hidden, cumulative_hidden)
            ###
            programmer_weight[:, i, :] = (word_emb_weight * word_attn.unsqueeze(2).expand_as(word_emb_weight)).sum(1)
            programmer_bias[:, i] = (word_emb_bias * word_attn).sum(1)
            ###
            cumulative_prog_hidden[:, i, :] = prog_hidden

        # 4) reasoning
        [_, _, height, width] = image_feature.size()
        extra_feature = self.extra_feature.expand(batch_size, self.extra_dim, height, width)
        concat_feature = torch.cat((image_feature, extra_feature), 1)
        ##
        # ground_state = Variable(torch.ones((batch_size, 1, height, width))) / 9.0
        ground_state = Variable(torch.zeros((batch_size, 1, height, width)))
        ground_state[:, :, int((height - 1) / 2), int((width - 1) / 2)] = 1
        ##
        message_gate = Variable(torch.zeros(batch_size, 1))
        ##
        # cumulative_attn = Variable(torch.zeros((batch_size, num_words, height, width)))
        for i in range(self.programmer_size):

            ## 4.1) generate kernel for new, attended word
            img_score = Variable(torch.zeros(batch_size, 1, height, width))

            ### conv2d(input: (batch_size, in_channel, h, w), weights: (out_channel, in_channel, filter_h, filter_w), ...)
            conv_filter = programmer_weight[:, i, :].contiguous().view(batch_size, -1, 1, 1)
            conv_bias = programmer_bias[:, i]
            for j in range(batch_size):
                img_score[j, :, :, :] = F.conv2d(concat_feature[j, :, :, :].unsqueeze(0),
                                                 conv_filter[j, :, :, :].unsqueeze(0), conv_bias[j])
            ### get kernel (normalized and flip)
            kernel = self.get_kernel(img_score)

            ## 4.2) pass messages
            last_ground_state, last_message_gate = ground_state, message_gate
            ground_state = Variable(torch.zeros((batch_size, 1, height, width)))
            ###
            message_gate = F.sigmoid(self.message(torch.cat((cumulative_prog_hidden[:, i,:].squeeze(1), last_message_gate), 1)))
            # message_gate = Variable(torch.ones(batch_size, 1))
            ###
            for j in range(batch_size):
                ## there is a gate: gate = 1 -> calculate the message f -> v
                ##                  gate = 0 -> calculate the message v -> f
                if mode == 'train':
                    ground_state[j, :, :, :] = message_gate[j] * F.conv2d(last_ground_state[j, :, :, :].unsqueeze(0), kernel[j, :, :, :].unsqueeze(0), None, 1, 1) + \
                                        (1 - message_gate[j]) * torch.mul(last_ground_state[j, :, :, :].unsqueeze(0), kernel[j, :, :, :].unsqueeze(0))
                else:
                    ground_state[j, :, :, :] = message_gate[j] * F.conv2d(last_ground_state[j, :, :, :].unsqueeze(0), kernel[j, :, :, :].unsqueeze(0), None, 1, 1) + \
                                        (1 - message_gate[j]) * torch.mul(last_ground_state[j, :, :, :].unsqueeze(0), kernel[j, :, :, :].unsqueeze(0))

        # 5) if vqa, reasoning the feature
        # print ground_state[0]
        if sentence_type == 'vqa':
            sum_ground_state = ground_state.view(batch_size, -1).sum(1)
            ground_state = ground_state / sum_ground_state.view(-1, 1, 1, 1).expand_as(ground_state)
            ##
            x = concat_feature * ground_state.expand_as(concat_feature)
            x = x.sum(3).sum(2)
            output_gate = F.sigmoid(self.output_gate(summary_hidden))
            # output_gate = torch.cat((Variable(torch.ones(batch_size, 18)), Variable(torch.zeros(batch_size, self.emb_dim-18))), 1)
            x = x * output_gate
            joint_prob = torch.mm(x, self.emb.transpose(0, 1)) + self.emb_bias.unsqueeze(0).expand(batch_size, self.n_words + 1)
            # joint_prob = F.softmax(y)  ## joint_prob here is pred

        elif sentence_type == 'caption':
            joint_prob = ground_state.sum(2).sum(2)

        return joint_prob

def train(model, cluster_center, color_center, difficulty, n_epoch=1000, sentence_type='vqa'):
    mode = 'train'
    optimizer = optim.Adam(model.parameters(), lr=0.01)
    for epoch in range(n_epoch):
        batch = data.generate_data(cluster_center, color_center, difficulty, 100) # batch_size = 20
        image_feature = Variable(torch.from_numpy(batch[0])).float()
        ##
        input_word_id, idx, output_word_id = preprocess.process_sentence(batch[1])
        input_word_id = Variable(torch.from_numpy(input_word_id)).long()
        idx = Variable(torch.from_numpy(idx)).long()
        output_word_id = Variable(torch.from_numpy(output_word_id)).long()
        ##
        input_word_id_vqa, idx_vqa, output_word_id_vqa = preprocess.process_question_answer(batch[2], batch[3])
        input_word_id_vqa = Variable(torch.from_numpy(input_word_id_vqa)).long()
        idx_vqa = Variable(torch.from_numpy(idx_vqa)).long()
        output_word_id_vqa = Variable(torch.from_numpy(output_word_id_vqa)).long()
        ##
        optimizer.zero_grad()
        if sentence_type == 'caption':
            pred = model(image_feature, input_word_id, idx, sentence_type, mode)
            loss = 1 - pred.mean()
        elif sentence_type == 'vqa':
            pred = model(image_feature, input_word_id_vqa, idx_vqa, sentence_type, mode)
            loss = F.cross_entropy(pred, output_word_id_vqa.squeeze_(1))

        loss.backward()
        optimizer.step()
        print('Train Epoch: {}\tLoss: {:.6f}'.format(epoch, loss.data[0]))
    return model

def test(model, cluster_center, color_center, difficulty, n_epoch=100):
    mode = 'test'
    total_loss = 0.0
    for epoch in range(n_epoch):
        batch = data.generate_data(cluster_center, color_center, difficulty, 20) # batch_size = 20
        image_feature = Variable(torch.from_numpy(batch[0])).float()
        input_word_id, idx, output_word_id = preprocess.process_sentence(batch[1])
        input_word_id = Variable(torch.from_numpy(input_word_id)).long()
        idx = Variable(torch.from_numpy(idx)).long()
        output_word_id = Variable(torch.from_numpy(output_word_id)).long()
        pred = model(image_feature, input_word_id, idx, mode)
        loss = 1 - pred.mean()
        total_loss = total_loss + loss.data[0]

    print('Epoch: {}\t Total Loss: {:.6f}'.format(epoch, total_loss/epoch))

    return total_loss

# def test(model, cluster_center, difficulty, n_epoch=10):
#     test_accuracy = []
#     test_attn_accuracy = []
#     for epoch in range(n_epoch):
#         batch = data.generate_data(cluster_center, difficulty)
#         image_feature = Variable(torch.from_numpy(batch[0])).float()
#         input_word_id, idx, output_word_id = preprocess.process_sentence(batch[1])
#         input_word_id = Variable(torch.from_numpy(input_word_id)).long()
#         idx = Variable(torch.from_numpy(idx)).long()
#         output_word_id = Variable(torch.from_numpy(output_word_id)).long()
#         sentences = batch[1]
#         questions, answers = batch[2], batch[3]
#         object_label, color_label = batch[4], batch[5]
#         output_location = preprocess.get_groundtruth_location(output_word_id, object_label, color_label)
#         output_location = Variable(torch.from_numpy(batch[4])).long()
#         pred, attn = model(image_feature, input_word_id, idx)
#         attn = attn.view(attn.size(0), -1)
#         pred = pred.data.max(1)[1]
#         attn = attn.data.max(1)[1]
#         accuracy = pred.eq(output_word_id.data).cpu().sum() * 1.0 / pred.size(0)
#         attn_accuracy = attn.eq(output_location.data).cpu().sum() * 1.0 / attn.size(0)
#         test_accuracy.append(accuracy)
#         test_attn_accuracy.append(attn_accuracy)
#     return numpy.mean(numpy.asarray(test_accuracy)), numpy.mean(numpy.asarray(test_attn_accuracy))


def main():
    word_dict, object_dict, color_dict, _, _, _ = data.generate_word_dictionary()
    object_center = data.generate_cluster_center(len(object_dict), 10)
    color_center = data.generate_cluster_center(len(color_dict), 8)
    img_size = 3  # Currently only support image with size 3x3

    ##
    paras = {'encoder_hidden_dim': 32, 'decoder_hidden_dim': 32, 'emb_dim': 36, 'n_words': len(word_dict), 'img_size': 3, 'img_dim': 18}
    ##
    # model = SimpleNet(len(word_dict), object_center.shape[1]+color_center.shape[1], img_size)
    model = Net3(paras)
    # Curriculum learning
    for difficulty in range(11):
    # for difficulty in [8]:
        print '...visual question answering..., difficulty = ' + str(difficulty)
        model = train(model, object_center, color_center, difficulty, sentence_type='vqa')
        total_loss = test(model, object_center, color_center, difficulty)
        # print('average accuracy: %.2f' % test_accuracy)
        # print('average attention accuracy: %.2f' % test_attn_accuracy)

if __name__ == '__main__':
    main()
