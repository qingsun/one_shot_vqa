import numpy
import data


def process_sentence(sentences, fill_forward = 0):
    """
    Change sentences into a torch array of word ids
    The shorter sentences will be padded with #oov#, with an idx indicating the end of sentence
    :param sentences: 
    :return: 
    """
    word_dict, _, _, _, _, _ = data.generate_word_dictionary()
    batch_size = len(sentences)
    max_sentence_length = max([len(s.split(' ')) for s in sentences])
    input_word_id = numpy.zeros((batch_size, max_sentence_length + 1))
    idx = numpy.zeros(batch_size)
    output_word_id = numpy.zeros((batch_size, max_sentence_length + 1))
    for i in range(len(sentences)):
        sentence = sentences[i].split(' ')
        sentence = [word_dict.get(word) for word in sentence]
        sentence = numpy.asarray(sentence).astype(int)
        # input_word_id[i, -1] = word_dict['#oov#']
        if fill_forward:
            input_word_id[i, -len(sentence)-1] = word_dict['#s#']
            input_word_id[i, -len(sentence):] = sentence
        else:
            input_word_id[i, 0] = word_dict['#s#']
            input_word_id[i, 1:len(sentence) + 1] = sentence

        ## output sequence
        if fill_forward:
            output_word_id[i, -1] = word_dict['#oov#']  # for image captioning, there is no "answer"
            output_word_id[i, -len(sentence)-1:-1] = sentence
        else:
            output_word_id[i, len(sentence)] = word_dict['#oov#']  # for image captioning, there is no "answer"
            output_word_id[i, 0:len(sentence)] = sentence
        # the index of the last word of the sentence
        if fill_forward:
            idx[i] = max_sentence_length  # the index of the last word of the sentence
        else:
            idx[i] = len(sentence)
    return input_word_id, idx, output_word_id


def process_question_answer(questions, answers, fill_forward = 0):
    """
    Change questions into a torch array of word ids
    The shorter questions will be padded with #oov#, with an idx indicating the end of question
    :param questions: 
    :param answers:
    :return: 
    """
    word_dict, _, _, _, _, _ = data.generate_word_dictionary()
    batch_size = len(questions)
    max_question_length = max([len(s.split(' ')) for s in questions])
    input_word_id = numpy.zeros((batch_size, max_question_length + 1))
    idx = numpy.zeros(batch_size)
    max_answer_length = max([len(s.split(' ')) for s in answers])
    output_word_id = numpy.zeros((batch_size, max_answer_length)) # assume the answer is just a single word
    for i in range(len(questions)):

        ## input sequence
        question = questions[i].split(' ')
        question = [word_dict.get(word) for word in question]
        question = numpy.asarray(question).astype(int)

        if fill_forward:
            input_word_id[i, -len(question)-1] = word_dict['#s#']
            input_word_id[i, -len(question):] = question
        else:
            input_word_id[i, 0] = word_dict['#s#']
            input_word_id[i, 1:len(question) + 1] = question

        ## output sequence
        answer = answers[i].split(' ')
        answer = [word_dict.get(word) for word in answer]
        answer = numpy.asarray(answer).astype(int)

        if fill_forward:
            # output_word_id[i, -1] = word_dict['#oov#']  # for image captioning, there is no "answer"
            output_word_id[i, -len(answer)-1:-1] = answer
        else:
            # output_word_id[i, len(answer)] = word_dict['#oov#']  # for image captioning, there is no "answer"
            output_word_id[i, 0:len(answer)] = answer

        # the index of the last word of the sentence
        if fill_forward:
            idx[i] = max_question_length  # the index of the last word of the sentence
        else:
            idx[i] = len(question)

    return input_word_id, idx, output_word_id


def process_question_answer_sent(questions, answers, fill_forward=0):
    """
    Change questions into a torch array of word ids
    The shorter questions will be padded with #oov#, with an idx indicating the end of question
    :param questions: 
    :param answers:
    :return: 
    """
    word_dict, _, _, _, _, _ = data.generate_word_dictionary()
    batch_size = len(questions)
    max_question_length = max([len(s.split(' ')) for s in questions])
    input_word_id = numpy.zeros((batch_size, max_question_length + 1))
    idx = numpy.zeros(batch_size)
    max_answer_length = max([len(s.split(' ')) for s in answers])
    output_word_id = numpy.zeros((batch_size, max_answer_length + 1))  # assume the answer is just a single word
    for i in range(len(questions)):

        ## input sequence
        question = questions[i].split(' ')
        question = [word_dict.get(word) for word in question]
        question = numpy.asarray(question).astype(int)

        if fill_forward:
            input_word_id[i, -len(question) - 1] = word_dict['#s#']
            input_word_id[i, -len(question):] = question
        else:
            input_word_id[i, 0] = word_dict['#s#']
            input_word_id[i, 1:len(question) + 1] = question

        ## output sequence
        answer = answers[i].split(' ')
        answer = [word_dict.get(word) for word in answer]
        answer = numpy.asarray(answer).astype(int)

        if fill_forward:
            output_word_id[i, -1] = word_dict['#oov#']  # for image captioning, there is no "answer"
            output_word_id[i, -len(answer) - 1:-1] = answer
        else:
            output_word_id[i, len(answer)] = word_dict['#oov#']  # for image captioning, there is no "answer"
            output_word_id[i, 0:len(answer)] = answer

        # the index of the last word of the sentence
        if fill_forward:
            idx[i] = max_question_length  # the index of the last word of the sentence
        else:
            idx[i] = len(question)


def main():
    word_dict, object_dict, color_dict, _, _, _ = data.generate_word_dictionary()
    object_center = data.generate_cluster_center(len(object_dict), 10)
    color_center = data.generate_cluster_center(len(color_dict), 8)
    # for difficulty in range(11):
    for difficulty in [11]:
        _, sentence, question, answer, object_label, color_label = \
            data.generate_data(object_center, color_center, difficulty, 10)
        print('difficulty %d' % difficulty)
        input_word_id, idx, output_word_id = process_sentence(sentence)
        print(sentence)
        print(input_word_id)
        print(idx)
        print(output_word_id)
        input_word_id, idx, output_word_id = process_question_answer(question, answer)
        print(question)
        print(answer)
        print(input_word_id)
        print(idx)
        print(output_word_id)


if __name__ == '__main__':
    main()
