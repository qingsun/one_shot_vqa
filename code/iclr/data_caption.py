import numpy


def generate_word_dictionary():
    """
    Generate word dictionary for caption and question-answer generation.
    The word dictionary contains all the words.
    :return: 
    """
    obj_words = ['apple', 'banana', 'cat', 'dog', 'elephant', 'frog', 'giraffe',
                    'horse', 'ice-cream', 'jeep', 'key', 'lion', 'monkey', 'nose']
    object_dict = dict(zip(obj_words, range(len(obj_words))))
    color_words = ['white', 'black', 'red', 'orange', 'yellow', 'green', 'cyan', 'blue', 'magenta']
    color_dict = dict(zip(color_words, range(len(color_words))))
    location_words = ['topleft', 'top', 'topright', 'left', 'middle', 'right', 'bottomleft',
                       'bottom', 'bottomright']
    location_dict = dict(zip(location_words, range(len(location_words))))
    number_words = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    number_dict = dict(zip(number_words, range(len(number_words))))
    question_words = ['what', 'what-color', 'where', 'how-many']
    question_dict = dict(zip(question_words, range(len(question_words))))
    preposition_words = ['is', 'on', 'in', 'of', 'the', 'a', 'image']
    oov_words = ['#oov#']
    all_words = obj_words + color_words + location_words + number_words + question_words
    all_words += preposition_words + oov_words
    word_dict = {}
    for i in range(len(all_words)):
        word_dict[all_words[i]] = i
    return word_dict, object_dict, color_dict, location_dict, number_dict, question_dict


def generate_other_dictionaries():
    """
    Currently assuming on a 3x3 grid, if you change the grid size, you may need to modify the 
    location dictionary.
    :return: 
    """
    location_dict = {(0, 0): 'topleft', (0, 1): 'top', (0, 2): 'topright',
                     (1, 0): 'left', (1, 1): 'middle', (1, 2): 'right',
                     (2, 0): 'bottomleft', (2, 1): 'bottom', (2, 2): 'bottomright'}
    relative_location_dict = {(-1, -1): 'topleft', (-1, 0): 'top', (-1, 1): 'topright',
                              (0, -1): 'left', (0, 0): 'middle', (0, 1): 'right',
                              (1, -1): 'bottomleft', (1, 0): 'bottom', (1, 1): 'bottomright'}
    number_dict = {0: '0', 1: '1', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9'}
    return location_dict, relative_location_dict, number_dict


def generate_sentence_format(difficulty=0):
    """
    Generate all possible caption formats
    :param difficulty: controls the question difficulty such as length and sentence types 
    :return: 
    """
    # {o}: object, {l}: location, {c}: color, {n}: number
    # preposition is predefined in the sentence format
    sentence_format = ['{o}', '{l} is {o}', 'image {l} is {o}', 'on image {l} is {o}',
                       '{o} is {l}', '{o} is on image {l}', '{o} is on the {l} of image',
                       '{o1} {l} is {o2}', '{l} of {o1} is {o2}',
                       '{o1} is on the {l} of {o2}', '{o1} is on {o2} {l}']
    return sentence_format[0:difficulty+1]


def generate_question_format(difficulty=0):
    """
    Generate all possible question formats
    :param difficulty: controls the question difficulty such as length and question types 
    :return: 
    """
    # {o}: object, {l}: location, {c}: color, {n}: number
    # question words and preposition is predefined in the sentence format
    question_format = ['what', '{l} is what', 'image {l} is what', 'on image {l} is what',
                       '{o} is where', '{o} is on image where', '{o} is on the where of image',
                       '{o1} {l} is what', '{l} of {o1} is what',
                       '{o1} is on the {l} of what', '{o1} is on {o2} where']
    return question_format[0:difficulty+1]


def generate_data(object_center, color_center, difficulty, batch_size=1, img_size=3):
    """
    Generate data, including image feature and sentences
    :param object_center: 
    :param color_center:
    :param difficulty:
    :param batch_size: 
    :param img_size: currently img_size can be only 3
    :return: 
    """
    image_feature, object_label, color_label = \
        generate_image_feature(object_center, color_center, batch_size, img_size)
    sentence = generate_sentence(object_label, color_label, difficulty, batch_size, img_size)
    question, answer = \
        generate_question_answer(object_label, color_label, difficulty, batch_size, img_size)
    return image_feature, sentence, question, answer, object_label, color_label


def generate_image_feature(object_center, color_center, batch_size, img_size):
    """
    Generate image feature, image features are in batch_size x feature_dim x height x width
    :return: 
    """
    (n_object, n_dim_obj) = object_center.shape
    (n_color, n_dim_col) = color_center.shape
    n_dim = n_dim_obj + n_dim_col
    image_feature = numpy.zeros((batch_size, n_dim, img_size, img_size))
    object_label = numpy.zeros((batch_size, img_size, img_size))
    color_label = numpy.zeros((batch_size, img_size, img_size))
    # Generate object features and their locations on the grid with different objects and colors
    # There will be no empty grids
    locations = numpy.mgrid[0:img_size, 0:img_size].reshape((2, img_size ** 2))
    for i in range(batch_size):
        assert n_object >= img_size**2, 'number of objects is smaller than the whole image size'
        assert n_color >= img_size**2, 'number of color is smaller than the whole image size'
        ids_obj = numpy.random.permutation(n_object)[0:img_size**2]
        ids_col = numpy.random.permutation(n_color)[0:img_size**2]
        for j in range(img_size**2):
            object_label[i, locations[0, j], locations[1, j]] = ids_obj[j]
            color_label[i, locations[0, j], locations[1, j]] = ids_col[j]
            object_feature = sample(object_center, ids_obj[j])
            color_feature = sample(color_center, ids_col[j])
            image_feature[i, :, locations[0, j], locations[1, j]] = \
                numpy.concatenate((object_feature, color_feature))
    return image_feature, object_label, color_label


def generate_sentence(object_label, color_label, difficulty, batch_size, img_size):
    """
    Generate sentences
    :param object_label: 
    :param color_label: 
    :param difficulty:
    :param batch_size: 
    :param img_size:
    :return: 
    """
    # Obtain word dictionary and other dictionaries
    word_dict, object_dict, color_dict, location_dict, number_dict, _ = generate_word_dictionary()
    reverse_object_dict = dict((v, k) for k, v in object_dict.iteritems())
    reverse_color_dict = dict((v, k) for k, v in color_dict.iteritems())
    location_dict, relative_location_dict, number_dict = generate_other_dictionaries()
    # Obtain sentence format, the sentence format complexity is controlled by difficulty
    sentence_format = generate_sentence_format(difficulty)
    sentences = []
    locations = numpy.mgrid[0:img_size, 0:img_size].reshape((2, img_size ** 2))
    for i in range(batch_size):
        # Sample first object
        rand_idx = numpy.random.randint(img_size**2)
        obj_location = locations[:, rand_idx]
        obj_word = reverse_object_dict[object_label[i, obj_location[0], obj_location[1]]]
        color_word = reverse_color_dict[color_label[i, obj_location[0], obj_location[1]]]
        location_word = location_dict[tuple(obj_location)]
        # Sample second object, need to be close to the first object
        obj2_location = numpy.random.randint(0, img_size, size=2)
        while all(obj2_location - obj_location == 0) or any(abs(obj2_location - obj_location) > 1):
            obj2_location = numpy.random.randint(0, img_size, size=2)
        obj2_word = reverse_object_dict[object_label[i, obj2_location[0], obj2_location[1]]]
        color2_word = reverse_color_dict[color_label[i, obj2_location[0], obj2_location[1]]]
        # Generate sentence
        sentence = sentence_format[numpy.random.randint(len(sentence_format))]
        if sentence == '{o}':
            obj_word = reverse_object_dict[object_label[i, 1, 1]]
            sentence = sentence.replace('{o}', obj_word)
        elif sentence in ['{l} is {o}', '{o} is {l}', 'image {l} is {o}', 'on image {l} is {o}',
                          '{o} is on image {l}', '{o} is on the {l} of image']:
            sentence = sentence.replace('{l}', location_word)
            sentence = sentence.replace('{o}', obj_word)
        elif sentence in ['{o1} {l} is {o2}', '{l} of {o1} is {o2}']:
            relative_location = obj2_location - obj_location
            relative_location_word = relative_location_dict[tuple(relative_location)]
            sentence = sentence.replace('{o1}', obj_word)
            sentence = sentence.replace('{l}', relative_location_word)
            sentence = sentence.replace('{o2}', obj2_word)
        elif sentence in ['{o1} is on the {l} of {o2}', '{o1} is on {o2} {l}']:
            relative_location = obj_location - obj2_location
            relative_location_word = relative_location_dict[tuple(relative_location)]
            sentence = sentence.replace('{o1}', obj_word)
            sentence = sentence.replace('{l}', relative_location_word)
            sentence = sentence.replace('{o2}', obj2_word)
        sentences.append(sentence)
    return sentences


def generate_question_answer(object_label, color_label, difficulty, batch_size, img_size):
    """
    To be Done
    :param object_label: 
    :param color_label: 
    :param difficulty: 
    :param batch_size: 
    :param img_size: 
    :return: 
    """
    # Obtain word dictionary and other dictionaries
    word_dict, object_dict, color_dict, location_dict, number_dict, _ = generate_word_dictionary()
    reverse_object_dict = dict((v, k) for k, v in object_dict.iteritems())
    reverse_color_dict = dict((v, k) for k, v in color_dict.iteritems())
    location_dict, relative_location_dict, number_dict = generate_other_dictionaries()
    # Obtain sentence format, the sentence format complexity is controlled by difficulty
    question_format = generate_question_format(difficulty)
    questions = []
    answers = []
    locations = numpy.mgrid[0:img_size, 0:img_size].reshape((2, img_size ** 2))
    for i in range(batch_size):
        # Sample first object
        rand_idx = numpy.random.randint(img_size**2)
        obj_location = locations[:, rand_idx]
        obj_word = reverse_object_dict[object_label[i, obj_location[0], obj_location[1]]]
        color_word = reverse_color_dict[color_label[i, obj_location[0], obj_location[1]]]
        location_word = location_dict[tuple(obj_location)]
        # Sample second object, need to be close to the first object
        obj2_location = numpy.random.randint(0, img_size, size=2)
        while all(obj2_location - obj_location == 0) or any(abs(obj2_location - obj_location) > 1):
            obj2_location = numpy.random.randint(0, img_size, size=2)
        obj2_word = reverse_object_dict[object_label[i, obj2_location[0], obj2_location[1]]]
        color2_word = reverse_color_dict[color_label[i, obj2_location[0], obj2_location[1]]]
        # Generate sentence
        question = question_format[numpy.random.randint(len(question_format))]
        if question == 'what':
            obj_word = reverse_object_dict[object_label[i, 1, 1]]
            answer = obj_word
        elif question in ['{l} is what', 'image {l} is what', 'on image {l} is what']:
            question = question.replace('{l}', location_word)
            answer = obj_word
        elif question in ['{o} is where', '{o} is on image where', '{o} is on the where of image']:
            question = question.replace('{o}', obj_word)
            answer = location_word
        elif question in ['{o1} {l} is what', '{l} of {o1} is what']:
            relative_location = obj2_location - obj_location
            relative_location_word = relative_location_dict[tuple(relative_location)]
            question = question.replace('{o1}', obj_word)
            question = question.replace('{l}', relative_location_word)
            answer = obj2_word
        elif question in ['{o1} is on the {l} of what']:
            relative_location = obj_location - obj2_location
            relative_location_word = relative_location_dict[tuple(relative_location)]
            question = question.replace('{o1}', obj_word)
            question = question.replace('{l}', relative_location_word)
            answer = obj2_word
        elif question in ['{o1} is on {o2} where']:
            relative_location = obj_location - obj2_location
            relative_location_word = relative_location_dict[tuple(relative_location)]
            question = question.replace('{o1}', obj_word)
            question = question.replace('{o2}', obj2_word)
            answer = relative_location_word
        questions.append(question)
        answers.append(answer)
    return questions, answers


def sample(cluster_center, y, var=0.1):
    return numpy.random.normal(cluster_center[y, :], var)


def generate_cluster_center(n_class, n_dim):
    cluster_center = numpy.random.uniform(-1, 1, size=(n_class, n_dim))
    if n_dim == 1:
        cluster_center = numpy.linspace(0, n_class - 1, n_class).reshape(-1, 1)
    return cluster_center


def unit_test():
    word_dict, object_dict, color_dict, _, _, _ = generate_word_dictionary()
    object_center = generate_cluster_center(len(object_dict), 10)
    color_center = generate_cluster_center(len(color_dict), 8)
    for difficulty in range(11):
        _, sentence, question, answer, _, _ = \
            generate_data(object_center, color_center, difficulty, 10)
        print('difficulty %d' % difficulty)
        print(sentence)
        print(question)
        print(answer)

if __name__ == '__main__':
    unit_test()
