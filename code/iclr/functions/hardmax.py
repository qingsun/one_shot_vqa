
import numpy
import torch
from torch.autograd import Variable
from torch.autograd import Function
from torch.nn.parameter import Parameter

# Inherit from Function
class Hardmax(Function):

    # Note that both forward and backward are @staticmethods
    # @staticmethod
    # bias is an optional argument
    def forward(self, input):
        ##
        index = input.max(1)[1]
        output = torch.zeros(input.shape)
        for i in range(index.size(0)):
            output[i, index[i]] = 1

        return output

    # This function has only a single output, so it gets only one gradient
    # @staticmethod
    def backward(self, grad_output):
        #
        grad_input = grad_output.clone()

        return grad_input



hardmax = Hardmax()

a = Variable(torch.from_numpy(numpy.array([[1, 3, 4], [8, 1, 0]]))).float()
grad = Variable(torch.from_numpy(numpy.array([[0.9, 1, 0.8], [0.5, 0.2, 0.1]]))).float()

#
weight = Parameter(torch.rand(3, 3)).float()
c = torch.mm(a, weight)

b = hardmax(c)
b.backward(grad)
print weight.grad.data
print 'calculate weight"s gradients'
print torch.mm(a.t(), grad)