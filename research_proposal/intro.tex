%!TEX root = paper.tex
\section{Introduction}

\textbf{Motivation.} Visual language grounding for robotics is an important research area in Artificial Intelligence. 
It is worth noting that a 4-5 year-old child has the remarkable ability to fast learn and accumulate visual knowledge by talking to their parents.
When building an intelligent robot, we want the robot to have this ability.
In this paper, we study the problem: how to develop a deep neural architecture that is able to fast and incrementally learn novel visual concepts through natural language descriptions.
We call it ``{\em Learning Naturally}", since it mimics the 4-5 year-old child's learning process.
Such system can be viewed as a crucial part for developing a human-like robot that is able to learn from people in a natural way (i.e. through talking).

\begin{figure}
\begin{center}
\includegraphics[width=0.9\linewidth]{../figs/how-to-teach-my-child-to-read}
\end{center}
\caption{How children learn (visual) concepts? Parents teach their children by speaking the name of the concepts and explaining the meaning of the concepts through natural language. Children learn the concepts by decomposing the language and match the correspondence between image and the concept. We hope a human-like learning robot also has this learning ability.} 
\label{fig:splash}
\end{figure}

\textbf{What is Learning Naturally?} Before staring explaining the methodology, we first define {\em Learning Naturally} to help the readers to understand our problem.
We define a robot learning system contains the natural learning ability if it can
\begin{itemize}
\item {\em Understanding language} - Parsing language in different formats. Parents teach and test their children using natural language in different formats, \eg~providing descriptions or asking questions. For example, when parents teach children visual concepts, they usually speak the full sentence such as \texttt{[This is an apple.]} or \texttt{[That is a pear.]}, instead of a single word \texttt{[Apple]} or \texttt{[Pear]}. This happens more frequently after the children gain their basic language understanding ability. Similarly, when parents test if children understand a concept, they ask a full question like \texttt{[What is this object?]} and expect the children to give the correct answer. The children must have the ability to transfer the learned concepts smoothly between captioning and question answering.
\item {\em Reasoning with language} - Reason with complex language and ground concepts into image with attention. Besides language parsing, children also have the ability to attend to the correct image region by utilizing the language context. For example, after hearing \texttt{[The pear is on top of the table and left of the apple.]}. The children can attend to the pear region based the context (assuming they already understand table and apple). This attention (reasoning) mechanism helps the children to extract more accurate and cleaner image features to better learn novel visual concepts.
\item {\em Learning fast} - Learn novel visual concepts fast. Why do we need this? Human is capable of learning and generalizing with small amount of data. Human intelligence differs from the recent large scale deep learning system in the way that they are able to learn novel visual concepts with only a small amount of training data. Fast learning and small data learning is also an ability that is necessary for a human-like robot. If we want the robot to learn in the real-world environment, it is hard to provide many training examples as the traditional way to train deep models.
\item {\em Learning incrementally} - Accumulate learned visual concepts incrementally. Why do we need this? Because life-long learning and incremental learning is also the ability in human intelligence. The human can gradually accumulate the learned knowledge through time. We expect a learning robot also have this ability.
\end{itemize}

\textbf{The overall illustration of our problem.}
Although we given a definition of learning naturally, what is the process really look like?
What is the first step to take to address this problem?
Figure~\ref{fig:splash} illustrates the process of natural learning.
In the natural learning task, the machine first receives images and their corresponding language descriptions from the human teacher.
And we expect the machine learns the mapping from images to some of the keywords in this process.
The machine then receives a test in a way as visual question answering.
And we expect the machine to provide the correct answer after the learning.
The whole process is communicated with natural languages.
Hence the whole flow is similar to human parents teaching and testing their children. 

Next, we provide an explanation about this design.

\textbf{Why use natural languages to learn image concepts?}
One may doubt since we can directly build an object recognition system through training image classification, why do we need to bother with learning visual concepts through natural languages.
We believe that for a real-world learning robot, when it learns through the interaction with human, using natural language is a more natural way than a single word.
And language makes it possible for a human teacher to communicate more complex information.  

\textbf{Why use image captioning to train?} Human answer questions correctly if and only if he/she has ever learned relevant knowledge and reasoning ability. Such knowledge and reasoning ability are usually obtained in various ways, \eg.~reading books, taking courses, asking questions, etc. Most of the information is provided through language descriptions, or namely image captioning. Image captioning is the largest source for people to learn from daily human activities.

\textbf{Why use visual question answering to test?} Visual question answering is a more effective way to learn and test if a machine has already understood the concept. For example, if we let a machine to give descriptions, both \texttt{[Someone's hand is holding on to a frisbee.]} and \texttt{[A cat is fighting with its owner for a frisbee.]} are meaningful captions, but we are not sure if the machine understand what a dog is. But we can ask \texttt{[What animal is pulling a frisbee?]}, the answer is \texttt{[dog]}. Image question answering is also the way how people conduct Turing Test.

It is worth to note that visual question answering is not the only way for a test. 
For example, we can also use image captioning data to test the machine.
Instead of asking the machine to generate the whole sentences, we can use ``{\em fill in the blank}" test, which asks the machine to predict the blank word by using all other words in the sentence. This is somewhat similar to visual question answering.

\begin{figure}
\begin{center}
\includegraphics[width=0.99\linewidth]{../figs/splash.pdf}
\end{center}
\caption{We aim to teach the machine to learn novel visual concepts through image captioning and test how the machine learns through image question answering. This is somewhat similar to human parents teaching and testing their children. In order to make correct prediction, the system needs to transfer knowledge between descriptions and question-answer pairs.} 
\label{fig:splash}
\end{figure}

\section{The Pathway Towards a Natural Learning System}

\textbf{Goal.} The goal of this work is to build a machine that fast and incrementally learn (novel) visual concepts though natural language, like a 4-5 year-old child. 
We achieve this goal via several steps with multiple milestones.
We carefully examine each milestone and provide a discussion to the possible solution.

\textbf{Challenges.} Learning to transfer knowledge between image captioning and image question answering is a new problem. 
Learning novel visual concepts though natural language is a very challenging problem. 
Learning fast and incrementally is another challenging problem. 
Overall, it is hard to imagine a simple deep model (i.e. convolutional neural networks or recurrent neural networks) can solve the whole problem.
We decide to address each problem individually and later combine these submodules into a unified end-to-end deep learning system.
To the best of our knowledge, we are the first attempt towards building such a system. 

\textbf{Approach.} We propose an approach that gradually reaches the goal by sequentially solve the intermediate milestones.  
\begin{itemize}
\item {\em Understanding language:} We define this problem as developing a unified language model that is able to transfer learned concepts between image captioning and visual question answering. 
Being able to transfer any words or phrases between image captioning and visual question answering is a view of such system can understand language.
Particularly, when a concept is learned on one task (\ie~image captioning) throughly, can we use it on another task directly (\ie~image question answering). 
One may doubt if this is a trivial problem.
But as we will show in the later experiments, our baseline multi-modal recurrent neural networks only give about 6\% accuracy on transferring knowledge learned on captioning to question answering.
It is urgent to develop a better model that is able to deeply understand both image captioning and image question answering.
\item {\em Reasoning with language:} We define this problem as developing a model that is able to reason with complex language descriptions and ground visual concepts into local image regions with attention. 
Reasoning is important for visual question answering.
It is also important for visual language grounding.
In order to learning a visual concept fast and robustly, we find the quality of the (grounded) image features is the key.
Hence, it is necessary to build a more sophisticated attention model which can attend to the correct image region by understanding context.
\item {\em Learning fast:} We define this problem as learning to one-shot learning. Given the grounded image feature, the fast novel concept learning problem therefore becomes one-shot image classification problem.
In the deep learning literature, the learning-to-learn framework is a potential way to learn novel image classification fast.
\item {\em Learning incrementally:} We define this problem as learning to accumulate knowledge through time. In this problem, how to overcome forgetness is key, since simply maximizing the classification accuracy on novel classes will inevitably decrease the old class accuracy without careful modeling.
\end{itemize}

\begin{figure}
\begin{center}
\includegraphics[width=0.99\linewidth]{../figs/framework}
\end{center}
\caption{We decompose our problem into 4 steps, where each one has its clear goal and problem. In the end, we combine all steps into a single end-to-end deep learning framework.} 
\label{fig:framework}
\end{figure}

Next we provide more details on addressing each problem.

\subsection{Knowledge transfer between image captioning and visual question answering}

Image captioning refers to the task of generating a sentence (a sequence of words) description given an image. 
Image question answering refers to the task of generating an answer (either a word or more flexibly a sentence) given an image and a question.
The two tasks look similar at the first glance.
For example, both tasks require the model to understand images including objects, attributes and their relationships.
Both tasks require the model to have language models to generate sentences or parse questions.
It is perhaps straightforward to share the same word embedding and same image model for both tasks.

However, there are also some key differences between the two tasks.
First, image captioning usually generates multiple words while answers in image question answering normally consist of very few words (\ie~Microsoft COCO dataset).
Under the Bayesian point of view, image captioning refers to maximizing the joint probability $P(w_1, w_2, \ldots, w_N | I)$ and image question answering refers to maximizing the conditional probability $P(w | I, w_1, w_2, \ldots, w_N)$, where $w_i$ is the $i$-th word, and $I$ represents the image feature.
Hence, if we simply share the two tasks with a multi-modal recurrent neural network, it is hard to imagine the two models can share all parameters easily under the discriminative learning framework.

Second, in image captioning, one image can correspond to multiple different captioning. 
While given a question, the answer is usually unique in image question answering.
Hence in the deep learning literature, the two models are generally different, as illustrated in Figure~\ref{fig:caption_vqa}.

One may think that the simplest way to share between the two tasks is to share all the model parameters but keep the different model structures.
This is our baseline model.
However, this baseline model only achieves 6\% transfer accuracy on Microsoft COCO dataset.
We will give a more concrete detail for how to conduct this experiment in the future section.
The take home message is that this does not work well.
It is hard to fully analyze the failure reason due to the black box property of deep neural networks.
But the results demonstrate that the simplest way does not allow the model to fully share the language model between the two tasks.

Now the question is: is there a better way to share the model between the two tasks?
The answer is {\em yes}, at least it seems possible to share knowledge in image captioning to image question answering, if we transfer the image captioning task to {\em fill-in-the-blank} task.
The problem in the {\em fill-in-the-blank} task is very similar to image question answering, except the blank words can occur at any position in the sentence.
For example, when training with \texttt{[An apple is on the table.]}.
We transfer it to \texttt{[An {\em blank} is on the table. apple.]}
This is some what very similar to the question-answer pair: \texttt{[What is on the table? apple.]}
Similar to question answering, the model in the {\em fill-in-the-blank} task also needs to predict a word given the surrounding context.

However, the above method simply transfer image captioning task into an image question answering task. But how to transfer knowledge learned in image question answering to image captioning?
It is necessary to develop a more systematic framework that can sharing the benefit between each other and apply it to all kinds of different language tasks.

When thinking in this direction, it is also worth to note that in different language systems (English v.s. Chinese), the hardness between transferring captioning and question answering is also different.
For example, in English, one may say \texttt{[An apple is on the table.]} and ask \texttt{[What is the apple on?]}
In Chinese, with the similar sentence, the question is \texttt{[An apple is on what?]} if we directly translate every word from Chinese to English without considering the grammar.

\begin{figure}
\begin{center}
\begin{tabular}{cc}
\includegraphics[width=0.45\linewidth]{../figs/image_caption} &
\includegraphics[width=0.45\linewidth]{../figs/vqa} \\
(a) image captioning model &
(b) image question answering model
\end{tabular}
\end{center}
\caption{The traditional recurrent neural network models for image captioning and image question answering. There are multiple variations and extension on these models, here we illustrate the simplest version. Red nodes indicate the input and green nodes indicate the output of the network.} 
\label{fig:caption_vqa}
\end{figure}

\subsection{Attention through reasoning with language:}

The 

\begin{figure}
\begin{center}
\begin{tabular}{cccc}
\includegraphics[width=0.2\linewidth]{../figs/attn_reason1} &
\includegraphics[width=0.2\linewidth]{../figs/attn_reason2} &
\includegraphics[width=0.2\linewidth]{../figs/attn_reason3} &
\includegraphics[width=0.2\linewidth]{../figs/attn_reason4} \\
{\small (a) original image} &
{\small (b) what is circle's color?} &
{\small (c) what is on circle's right?} &
{\small (d) what is below circle?}
\end{tabular}
\end{center}
\caption{We illustrates the attention mechanism with different question inputs during image question answering. For example, when reasoning with question ``what is on circle's right?", the model may attend to circle and then to circle's right.} 
\label{fig:attention_reasoning}
\end{figure}

\subsection{Learning to one-shot:}

\subsection{Learning to accumulate knowledge through time:}

requires to parse scene graphs, which is a structured problem. The structure information in natural languages can help machine understand new concepts better than simple image and class label pairs. Given a description \texttt{[ A yellow dog tries to pull a frisbee out of a person's hand]} as an example, relation-object tuples (\texttt{dog, pull, frisbee}) and (\texttt{frisbee, out of, hand}) produced in this description can help machine understand what dog really is. 2) Learning to answer questions from image captioning requires learning to learn how to "transfer" representations.

\textcolor{red}{
\textbf{Contributions.} Our paper makes the following contributions:
First, we first propose a framework to let human teach machine new concepts using natural languages, which is an pretty efficient and natural way. (Human can directly impart the concept/knowledge to machine rather than letting machine to automatically learn form huge data). Human only need to give a few examples, machine can rapidly learn. 
Second, we first extend traditional learning to learn which learns new concepts in the same task (\eg, image classification) to multiple tasks (\eg, image captioning $\to$ vqa).}

Our contribution is three fold:
First, we propose a deep recurrent model with augmented memory that learns novel visual concept through consecutive image captions. 
Second, we further extend the model with a module that provide visual question answering to test the novel visual concept learning. 
Third, the whole model is an end-to-end deep neural system.
To the best of our knowledge, this is the first model that proposes to learning to learn and test visual concept learning through natrual languages. 
We test our model on a synthetic visual captioning and visual question answering dataset, as well as on Microsoft Coco dataset that contains natural images and natural language descriptions.
Experimental results show that our model has the capability of learning to learn novel visual concepts as well as providing novel visual answers through natural languages. 